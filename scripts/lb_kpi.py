#!/usr/bin/python3
"""
    This script sends KPI of the lb service

"""
import argparse
import configparser
import sys
import logging
import logging.config
import json
from datetime import datetime, timedelta
import os
import requests

from opensearchpy import OpenSearch


def load_config():
    """  Load configuration  """
    primary_conf_path = "./lb.conf"
    secondary_conf_path = "/etc/cgs/cgs.conf"

    conf = configparser.ConfigParser()

    if os.path.isfile(primary_conf_path):
        logging.config.fileConfig(
            primary_conf_path, disable_existing_loggers=False)
        logger = logging.getLogger(__name__)
        logger.info("Reading configuration from %s.", primary_conf_path)
        conf.read(primary_conf_path)
    elif os.path.isfile(secondary_conf_path):
        logging.config.fileConfig(
            secondary_conf_path, disable_existing_loggers=False)
        logger = logging.getLogger(__name__)
        logger.info("Reading configuration from %s.", secondary_conf_path)
        conf.read(secondary_conf_path)
    else:
        sys.stderr.write("No configuration file found! (Should be either %s or %s.)" %
                         (primary_conf_path, secondary_conf_path))
        sys.exit(1)

    return conf


def get_arguments():
    """ Parse command line arguments"""
    parser = argparse.ArgumentParser(
        description='Gather heavy users of lxplus.')
    parser.add_argument('--from', dest='start_date', default=None,
                        help='starting date')
    parser.add_argument('--to', dest='end_date', default=None,
                        help='end date')
    parser.add_argument('--debug', dest='debug', action='store_true',
                        help='write debug messages',
                        default=False)

    args = parser.parse_args()
    return args


def send(document):
    """ send the document """
    return requests.post('http://monit-metrics:10012/',
                         data=json.dumps(document),
                         headers={"Content-Type": "application/json; charset=UTF-8"})

def get_number_of_cnames(logger, tenant, CACHEFILE):
    """ Gets how many cnames are defined in ermis for a given tenant using a json cache file with the lbd configuration generated from the SLS script"""
    logger.info("Getting the number of cnames from kermis for tenant %s", tenant)

    return int(os.popen('/usr/bin/cat ' + CACHEFILE + '| /usr/bin/jq \'map(select(.tenant==\"' + tenant + '\").cnames[]) |length\'').read())

def get_data(logger, args):
    """ Gets the KPI for the selected period"""
    logger.info("Ready to get the data for %s", args)

    my_conf = load_config()

    username = my_conf.get("elasticsearch", "user_lb")
    password = my_conf.get("elasticsearch", "password_lb")

    epoch = int(1000*(datetime.utcnow() - datetime(1970, 1, 1)).total_seconds())
    my_data = []

    try:
        my_es = OpenSearch([f"https://{username}:{password}@os-timber.cern.ch/os"],
                              use_ssl=True, verify_certs=True,
                              ca_certs="/etc/pki/tls/certs/ca-bundle.trust.crt")
        result = my_es.search(index="monit_prod_loadbalancer_logs_lbd_config*",
                              body={"size": 0,
                                    "query": {"bool": {"must": [{"range": {"metadata.timestamp": {"gte": "now-24h"}}},
                                                                {"exists": {"field": "data.node" }}]}},
                                    "aggs": {"tenant": {
                                        "terms": {"field": "data.partition_name"},
                                        "aggs": {"clusters": {"cardinality": {"field": "data.cluster"}},
                                                 "nodes": {"cardinality":{"field": "data.node"}}
                                                 }}}})
        logger.info(result['aggregations'])
        tenants = {}
        for data in result['aggregations']['tenant']['buckets']:
            logger.info(data)
            cnames_in_tenant = get_number_of_cnames(logger, data['key'], "/afs/cern.ch/user/s/slsmon/git/golbd/scripts/lbd_state_for_kpi.json")
            tenants[data['key']] = {
                'number_of_clusters': data['clusters']['value'],
                'number_of_cnames': cnames_in_tenant,
                'number_of_nodes': data['nodes']['value'],
            }

        for tenant in tenants:
            logger.info("tenant " + tenant)

            my_data.append({'timestamp': epoch,
                            'producer': 'loadbalancer',
                            'idb_fields': ["number_of_clusters", "number_of_cnames",
                                           "number_of_nodes"],
                            'idb_tags': ['partition_lb'],
                            'type': 'kpi',
                            'number_of_clusters': tenants[tenant]['number_of_clusters'],
                            'number_of_nodes': tenants[tenant]['number_of_nodes'],
                            'number_of_cnames': tenants[tenant]['number_of_cnames'],
                            'partition_lb': tenant})

    except KeyError as my_ex:
        logger.error("Error connecting to elasticsearch: %s", my_ex)

    return my_data


def send_kpi(logger, data):
    """ Get the kpi values and sends them to MONIT"""

    logger.info("Ready to send %s", data)
    response = send(data)
    logger.info("Sent the message with %i", response.status_code)
    if response.status_code != 200:
        logger.error("Error sending the kpi to monit")
        return False
    return True


def main():
    """ Let's do the the alarms"""
    args = get_arguments()
    logger = logging.getLogger(__name__)
    logger.propagate = False
    if args.debug:
        logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
            '%(asctime)s - %(name)s %(funcName)20s() - %(levelname)s - %(message)s')
    else:
        logger.setLevel(logging.INFO)
        formatter = logging.Formatter(
            '%(asctime)s -  %(levelname)s - %(message)s')
    chan = logging.StreamHandler()
    chan.setFormatter(formatter)
    logger.addHandler(chan)

    data = get_data(logger, args)

    send_kpi(logger, data)

    logger.info("Done")
    return 0


if __name__ == '__main__':
    sys.exit(main())
