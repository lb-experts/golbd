module lb-experts/golbd

go 1.22.0

toolchain go1.22.7

require (
	github.com/miekg/dns v1.1.62
	github.com/reguero/go-snmplib v0.0.0-20200224112611-c71fe8c5aaf6
	gitlab.cern.ch/lb-experts/golbd v0.0.0-00010101000000-000000000000
)

require (
	golang.org/x/mod v0.22.0 // indirect
	golang.org/x/net v0.32.0 // indirect
	golang.org/x/sync v0.10.0 // indirect
	golang.org/x/sys v0.28.0 // indirect
	golang.org/x/tools v0.28.0 // indirect
)

replace gitlab.cern.ch/lb-experts/golbd => ./
